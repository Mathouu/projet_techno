package mathieu.projet_tech;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.rssample.ScriptC_colorize;
import com.android.rssample.ScriptC_grey;
import com.android.rssample.ScriptC_invert;
import com.android.rssample.ScriptC_keepHue;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Mathieu
 */
public class MainActivity extends AppCompatActivity {

    /**
     * L'image que l'on souhaite traiter.
     */
    private Bitmap image;
    /**
     * L'image que l'on souhaite afficher apres application d'une fonction.
     */
    private Bitmap image_copy;
    /**
     * Load l'image que l'on veut.
     */
    private ImageView imgView;
    /**
     * ArrayList de boutons.
     *
     * @see Button
     */
    public ArrayList<Button> buttonList;


    /**
     * Permet d'initialiser les boutons.
     */
    public void initiateButton() {

        buttonList = new ArrayList<>();
        Button tb;
        tb = findViewById(R.id.optionButton);
        buttonList.add(tb);
        tb = findViewById(R.id.imageButton);
        buttonList.add(tb);

    }

    /**
     * Permet d'ajouter des actions à des boutons.
     */
    public void addListenerOnButton() {

        if (buttonList != null) {
            buttonList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    optionsPage(view);
                }
            });
            buttonList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imagePage(view);
                }
            });
        }
    }

    public void optionsPage(View view) {
        startActivity(new Intent(this, Options.class));
    }

    public void imagePage(View view) {
        startActivity(new Intent(this, Image.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("CV", "onCreate()");

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;

        // ANIMATION golri
        /*Animation zoomAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom);
        imgView.startAnimation(zoomAnimation);*/

        // Permet d'initier tout les boutons
        initiateButton();
        addListenerOnButton();

    }

    /**
     * @param bmp
     * @param img
     */
    public void undo(Bitmap bmp, ImageView img) {
        img.setImageBitmap(bmp);
    }

    /**
     * Permet de transformer une image en noir et blanc.
     * En utilisant getPixel/setPixel, qui prennent du temps a s'éxécuter.
     *
     * @param bmp L'image.
     * @see Bitmap#getPixel(int, int)
     * @see Bitmap#setPixel(int, int, int)
     */
    public void toGray(Bitmap bmp) {
        int pixel, redValue, greenValue, blueValue, gray;
        for (int y = 0; y < bmp.getHeight(); y++) {
            for (int x = 0; x < bmp.getWidth(); x++) {

                pixel = bmp.getPixel(x, y);

                redValue = Color.red(pixel);
                greenValue = Color.green(pixel);
                blueValue = Color.blue(pixel);

                gray = (int) Math.round(0.3 * redValue + 0.59 * greenValue + 0.11 * blueValue);
                bmp.setPixel(x, y, Color.rgb(gray, gray, gray));

            }
        }
    }

    /**
     * Permet de transformer une image en noir et blanc.
     * En utilisant getPixels/setPixels
     *
     * @param bmp L'image.
     * @see Bitmap#getPixels(int[], int, int, int, int, int, int)
     * @see Bitmap#setPixels(int[], int, int, int, int, int, int)
     */
    public void toGray2(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int pixel, gray;
        int[] pixels = new int[w * h]; // new table
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        for (int i = 0; i < h * w; i++) {

            pixel = pixels[i]; // on prends le pixel
            gray = (int) Math.round(0.3 * Color.red(pixel) + 0.59 * Color.green(pixel) + 0.11 * Color.blue(pixel));

            pixels[i] = Color.rgb(gray, gray, gray);
        }

        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }

    /**
     * Permet de transformer en noir et blanc une image en version renderScript.
     *
     * @param bmp L'image.
     */
    public void toGreyRS(Bitmap bmp) {

        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_grey greyScript = new ScriptC_grey(rs);

        greyScript.forEach_toGrey(input, output);

        output.copyTo(bmp);

        input.destroy();
        output.destroy();
        greyScript.destroy();
        rs.destroy();
    }

    /**
     * Permet d'inverser les couleurs d'une image en version renderScript.
     *
     * @param bmp L'image.
     */
    public void invertRS(Bitmap bmp) {

        // 1) Creer un contexte RenderScript
        RenderScript rs = RenderScript.create(this);

        // 2) Creer des Allocations pour passer les donnees
        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // 3) Creer le script
        ScriptC_invert invertScript = new ScriptC_invert(rs);

        // 4) Copier les donnees dans les Allocations
        // ...
        // 5) Initialiser les variables globales potentielles
        // ...
        // 6) Lancer le noyau

        invertScript.forEach_invert(input, output);

        // 7) Recuperer les donnees des Allocation (s)
        output.copyTo(bmp);

        // 8) Detruire le context , les Allocation (s) et le script
        input.destroy();
        output.destroy();
        invertScript.destroy();
        rs.destroy();
    }

    /**
     * Permet de colorizer une image avec une une teinte aléatoire en version renderScript.
     *
     * @param bmp L'image.
     */
    private void colorizeRS(Bitmap bmp) {
        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_colorize colorizeScript = new ScriptC_colorize(rs);

        Random r = new Random();
        int hue = r.nextInt(360);
        colorizeScript.set_hue(hue);

        colorizeScript.forEach_colorize(input, output);

        output.copyTo(bmp);

        input.destroy();
        output.destroy();
        colorizeScript.destroy();
        rs.destroy();
    }

    // Garde seulement une certaine teinte, passée en paramètre, sur une image
    private void keepHue(Bitmap bmp, int hue) {
        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_keepHue keepHueScript = new ScriptC_keepHue(rs);

        keepHueScript.set_hue(hue);
        keepHueScript.forEach_keepHue(input, output);

        output.copyTo(bmp);

        input.destroy();
        output.destroy();
        keepHueScript.destroy();
        rs.destroy();
    }


    /**
     * Permet de colorizer une image avec une une teinte aléatoire.
     *
     * @param bmp L'image.
     */
    public void colorize(Bitmap bmp) {
        // Hue (teinte) random

        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        float[] hsv = new float[3];
        float hue_random = (float) Math.random() * 359;

        for (int i = 0; i < h * w; i++) {

            Color.colorToHSV(pixels[i], hsv);
            hsv[0] = hue_random;
            pixels[i] = Color.HSVToColor(hsv);

        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }

    /**
     * Permet de garder le rouge d'une image et de mettre les autres couleurs en gris.
     *
     * @param bmp L'image.
     */
    public void keepRed(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        for (int i = 0; i < h * w; i++) {

            // On prends les references couleurs
            int r = Color.red(pixels[i]);
            int g = Color.green(pixels[i]);
            int b = Color.blue(pixels[i]);

            // Rouge
            if (r < g + b) {
                int gray = (int) Math.round(0.3 * Color.red(pixels[i]) + 0.59 * Color.green(pixels[i]) + 0.11 * Color.blue(pixels[i]));
                pixels[i] = Color.rgb(gray, gray, gray);
            }

        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
        imgView.setImageBitmap(bmp);

    }

    /**
     * Permet de garder le bleu d'une image et de mettre les autres couleurs en gris.
     *
     * @param bmp L'image.
     */
    public void keepBlue(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        float[] hsv = new float[3];

        for (int i = 0; i < h * w; i++) {

            // On prends les references couleurs
            int r = Color.red(pixels[i]);
            int g = Color.green(pixels[i]);
            int b = Color.blue(pixels[i]);

            if (b < r + g) {
                int gray = (int) Math.round(0.3 * Color.red(pixels[i]) + 0.59 * Color.green(pixels[i]) + 0.11 * Color.blue(pixels[i]));
                pixels[i] = Color.rgb(gray, gray, gray);
            }

        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
        imgView.setImageBitmap(bmp);
    }

    /**
     * Permet de garder le vert d'une image et de mettre les autres couleurs en gris.
     *
     * @param bmp L'image.
     */
    public void keepGreen(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        for (int i = 0; i < h * w; i++) {

            // On prends les references couleurs
            int r = Color.red(pixels[i]);
            int g = Color.green(pixels[i]);
            int b = Color.blue(pixels[i]);

            if (g < b + r) {
                int gray = (int) Math.round(0.3 * Color.red(pixels[i]) + 0.59 * Color.green(pixels[i]) + 0.11 * Color.blue(pixels[i]));
                pixels[i] = Color.rgb(gray, gray, gray);
            }

        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
        imgView.setImageBitmap(bmp);
    }

    /**
     * Permet d'avoir la plus petite valeur d'un tableau.
     *
     * @param tab Un tableau d'int.
     * @return La plus petite valeur.
     */
    public int minTab(int[] tab) {
        int min = 0;
        while (tab[min] == 0) {
            min++;
        }
        return min;
    }

    /**
     * Permet d'avoir la plus grande valeur d'un tableau compris entre [0;255].
     *
     * @param tab Un tableau d'int.
     * @return La plus grande valeur.
     */
    public int maxTab(int[] tab) {
        int max = 255;
        while (tab[max] == 0) {
            max--;
        }
        return max;
    }

    /**
     * Permet d'avoir la plus petite valeur d'un tableau.
     *
     * @param tab tableau d'int en 2 dimensions.
     * @param i   la position du tableau.
     * @return La plus petite valeur du tableau.
     */
    public int minTab(int[][] tab, int i) {
        int min = 0;
        while (tab[i][min] == 0) {
            min++;
        }
        return min;
    }

    /**
     * Permet d'avoir la plus grande valeur d'un tableau compris entre [0;255].
     *
     * @param tab tableau d'int en 2 dimensions.
     * @param i   la position du tableau.
     * @return La plus grande valeur du tableau.
     */
    public int maxTab(int[][] tab, int i) {
        int max = 255;
        while (tab[i][max] == 0) {
            max--;
        }
        return max;
    }

    /**
     * Permet de retourner l'histogramme(un tableau en 2 dimensions) d'une image en rgb (sur 8 bits).
     * Avec tab[0][x] = pour la composantes red.
     * Avec tab[1][x] = pour la composantes blue.
     * Avec tab[2][x] = pour la composantes green.
     *
     * @param bmp L'image.
     * @return Un tableau en 2 dimensions contenant les valeurs de l'histogramme.
     */
    public int[][] histRGB(Bitmap bmp) {
        int h = bmp.getHeight();
        int w = bmp.getWidth();
        int[] pixels = new int[w * h];
        int[][] hist = new int[3][256];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        for (int i = 0; i < h * w; i++) {
            // Composante red
            hist[0][Color.red(pixels[i])]++;
            // Composante green
            hist[1][Color.green(pixels[i])]++;
            // Composante blue
            hist[2][Color.blue(pixels[i])]++;
        }
        return hist;

    }

    /**
     * Permet de retourner l'histogramme(un tableau) d'une image en gris.
     *
     * @param bmp L'image.
     * @return Un tableau contenant les valeurs de l'histogramme.
     */
    public int[] histGrey(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        int[] hist = new int[256];
        int grey, pixel;
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);
        for (int i = 0; i < h * w; i++) {
            pixel = pixels[i];
            grey = (int) Math.round(0.3 * Color.red(pixel) + 0.59 * Color.green(pixel) + 0.11 * Color.blue(pixel));
            hist[grey]++;
        }

        return hist;
    }

    /**
     * Permet de retourner l'histogramme cumule d'une image en gris.
     *
     * @param bmp L'image.
     * @return Un tableau contenant les valeurs de l'histogramme cumule.
     */
    public int[] histCumuleGrey(Bitmap bmp) {

        int[] hist = histGrey(bmp);
        int[] histCumul = new int[256];
        histCumul[0] = hist[0];
        for (int i = 1; i < 256; i++) {
            histCumul[i] = histCumul[i - 1] + hist[i];
        }
        return histCumul;
    }

    /**
     * Permet de retourner l'histogramme cumule(un tableau en 2 dimensions) d'une image en rgb(sur 8 bits).
     * Avec tab[0][x] = pour la composantes red.
     * Avec tab[1][x] = pour la composantes blue.
     * Avec tab[2][x] = pour la composantes green.
     *
     * @param bmp L'image.
     * @return Un tableau en 2 dimensions contenant les valeurs de l'histogramme cumule.
     */
    public int[][] histCumuleRGB(Bitmap bmp) {

        int hist[][] = histRGB(bmp);
        int[][] histCumul = new int[3][256];
        histCumul[0][0] = hist[0][0]; // red
        histCumul[1][0] = hist[1][0]; // green
        histCumul[2][0] = hist[2][0]; // blue

        for (int i = 1; i < 256; i++) {
            histCumul[0][i] = histCumul[0][i - 1] + hist[0][i]; // red
            histCumul[1][i] = histCumul[1][i - 1] + hist[1][i]; // green
            histCumul[2][i] = histCumul[2][i - 1] + hist[2][i]; // blue
        }
        return histCumul;
    }

    // Extension de dynamique

    /**
     * Permet d'ameliorer le contraste d'une image en gris avec une extension lineaire.
     *
     * @param bmp L'image
     */
    public void extensionDynamicGrey(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();

        // Pour mettre l'image en gris si elle ne l'est pas
        toGreyRS(bmp);

        int[] hist = histGrey(bmp);
        int min = minTab(hist);
        int max = maxTab(hist);
        int[] LUT = new int[256];
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        // Calcul de la lut (table de transformation)
        for (int ng = 0; ng < 256; ng++) {
            if (max == 0 || (ng - min == 0)) {
                LUT[ng] = 0;
            } else {
                LUT[ng] = (255 * (ng - min)) / (max - min);
            }
        }

        // Application de la lut
        int grey;
        for (int i = 0; i < h * w; i++) {
            grey = LUT[Color.red(pixels[i])];
            // On change le pixel
            pixels[i] = Color.rgb(grey, grey, grey);
        }

        bmp.setPixels(pixels, 0, w, 0, 0, w, h);

    }

    /**
     * Permet d'ameliorer le contraste d'une image en gris avec une extension lineaire.
     *
     * @param bmp L'image.
     * @param min La borne min de la plage de l'extension.
     * @param max La borne max de la plage de l'extension.
     */
    public void extensionDynamicGreyV2(Bitmap bmp, int min, int max) {

        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] LUT = new int[256];
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        // Calcul de la lut (table de transformation)
        for (int ng = 0; ng < 256; ng++) {
            float x = (float) ng / 255;
            LUT[ng] = (int) (min + ((max - min) * x));
        }

        // Application de la lut
        int grey;
        for (int i = 0; i < h * w; i++) {
            grey = LUT[Color.red(pixels[i])];
            // On change le pixel
            pixels[i] = Color.rgb(grey, grey, grey);
        }

        bmp.setPixels(pixels, 0, w, 0, 0, w, h);

    }

    /**
     * Permet d'ameliorer le contraste d'une image rgb(sur 8 bits) avec une extension lineaire.
     *
     * @param bmp L'image.
     */
    public void extensionDynamicRGB(Bitmap bmp) {

        int[][] hist = histRGB(bmp);
        // Composante red
        int min0 = minTab(hist, 0);
        int max0 = maxTab(hist, 0);
        // Composante green
        int min1 = minTab(hist, 1);
        int max1 = maxTab(hist, 1);
        // Composante blue
        int min2 = minTab(hist, 2);
        int max2 = maxTab(hist, 2);

        int i;

        // Initialisation de la lut(table de transformation)
        int[][] LUT = new int[3][256];
        for (i = 0; i < 256; i++) {
            LUT[0][i] = (255 * (i - min0)) / (max0 - min0);
            LUT[1][i] = (255 * (i - min1)) / (max1 - min1);
            LUT[2][i] = (255 * (i - min2)) / (max2 - min2);
        }

        // Application de la lut
        int h = bmp.getHeight();
        int w = bmp.getWidth();
        int[] pixels = new int[w * h];
        int red, green, blue;
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        for (i = 0; i < h * w; i++) {
            // Composante red
            red = LUT[0][Color.red(pixels[i])];
            // Composante green
            green = LUT[1][Color.green(pixels[i])];
            // Composante blue
            blue = LUT[2][Color.blue(pixels[i])];
            // On change le pixel
            pixels[i] = Color.rgb(red, green, blue);
        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }

    // Egalisation d'histogramme

    // Ca marche pas trop

    /**
     * Permet d'ameliorer le contraste d'une image grise avec l'égalisation de son histogramme.
     *
     * @param bmp L'image.
     */
    public void egalisationHistGrey(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] LUT = new int[256];
        int[] pixels = new int[w * h];
        int[] histCumul = histCumuleGrey(bmp);

        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        for (int ng = 0; ng < 256; ng++) {
            LUT[ng] = histCumul[ng] * 255 / (w * h);
        }

        int grey;
        for (int i = 0; i < w * h; i++) {
            grey = LUT[Color.red(pixels[i])];
            pixels[i] = Color.rgb(grey, grey, grey);
        }

        bmp.setPixels(pixels, 0, w, 0, 0, w, h);

    }

    // Ca marche pas trop

    /**
     * Permet d'ameliorer le contraste d'une image RGB(8 bits) avec l'égalisation de son histogramme.
     *
     * @param bmp L'image.
     */
    public void egalisationHistRGB(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        int[][] LUT = new int[3][256];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        int[][] histCumul = histCumuleRGB(bmp);
        int red, green, blue;

        for (int ng = 0; ng < 256; ng++) {
            LUT[0][ng] = histCumul[0][ng] * 255 / (w * h);
            LUT[1][ng] = histCumul[1][ng] * 255 / (w * h);
            LUT[2][ng] = histCumul[2][ng] * 255 / (w * h);

        }
        for (int i = 0; i < w * h; i++) {
            green = LUT[1][Color.green(pixels[i])];
            red = LUT[0][Color.red(pixels[i])];
            blue = LUT[2][Color.blue(pixels[i])];
            pixels[i] = Color.rgb(red, green, blue);
        }

        bmp.setPixels(pixels, 0, w, 0, 0, w, h);

    }

    // Test
    public void egalisationRGB(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        // Calcul intensite
        int[] I = new int[w * h];
        for (int i = 0; i < w * h; i++) {
            I[i] = (Color.red(pixels[i]) + Color.green(pixels[i]) + Color.blue(pixels[i])) / 3;
        }
        // hist I
        int[][] hist = new int[3][256];
        for (int i = 0; i < h * w; i++) {
            hist[0][Color.red(I[i])]++;
            hist[1][Color.green(I[i])]++;
            hist[2][Color.blue(I[i])]++;
        }
        //hist cumule I
        int[][] histCumul = histCumuleRGB(bmp);

        int red, green, blue;

        for (int i = 0; i < w * h; i++) {

            red = (histCumul[0][Color.red(I[i])] * 255) / (w * h);
            green = (histCumul[1][Color.green(I[i])] * 255) / (w * h);
            blue = (histCumul[2][Color.blue(I[i])] * 255) / (w * h);
            pixels[i] = Color.rgb(red, green, blue);

        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);

    }

    /**
     * Permet d'appliquer un filtre 3*3 sur une image.
     *
     * @param bmp    L'image.
     * @param filtre Le filtre.
     */
    public void convolution(Bitmap bmp, float[][] filtre) {

        int margin = filtre.length / 2;
        int size = filtre.length * filtre.length;
        int center = size / 2;
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] pixels = new int[w * h];
        bmp.getPixels(pixels, 0, w, 0, 0, w, h);

        float[] res = new float[size];
        int red, green, blue, cmpt_row = 0;

        for (int i = 0; i < w * h; i++) {
            // Si on change de ligne
            if (i % w == 0) {
                cmpt_row++;
            }
            // Contours dans tout les cas
            if (i > w * margin && i < (h * w) - (w * margin) - 1 && i > (w * (cmpt_row - 1) - 1 + margin) && i < (w * cmpt_row - margin)) {
                // A changer
                for (int j = 0; j < margin; j++) {
                    // Centre
                    res[center] = pixels[i];
                    // Case cote droit
                    res[center + 1] = pixels[i + 1];
                    // Case cote gauche
                    res[center - 1] = pixels[i - 1];
                    // Case au dessus de centre
                    res[center - filtre.length - (filtre.length * j)] = pixels[i - w - (w * j)];
                    // Case en dessous du centre
                    res[center + filtre.length + (filtre.length * j)] = pixels[i + w + (w * j)];
                    // Coin en haut
                    res[center - filtre.length - (filtre.length * j) - 1] = pixels[i - w - (w * j) - 1];
                    res[center - filtre.length - (filtre.length * j) + 1] = pixels[i - w - (w * j) + 1];
                    // Coin en bas
                    res[center + filtre.length + (filtre.length * j) - 1] = pixels[i + w + (w * j) - 1];
                    res[center + filtre.length + (filtre.length * j) + 1] = pixels[i + w + (w * j) + 1];
                }

                red = 0;
                green = 0;
                blue = 0;
                for (int j = 0; j < size; j++) {
                    red += Color.red((int) res[j]);
                    green += Color.green((int) res[j]);
                    blue += Color.blue((int) res[j]);
                }
                red = red / 9;
                green = green / 9;
                blue = blue / 9;

                pixels[i] = Color.rgb(red, green, blue);
            }
        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }

    public void printHist(float[] hist) {
        for (int i = 0; i < 256; i++) {
            System.out.println(" " + i + " " + hist[i]);
        }
    }

    public void printHist(int[] hist) {
        for (int i = 0; i < 256; i++) {
            System.out.println(" " + i + " " + hist[i]);
        }
    }

    public void printHist(double[] hist) {
        for (int i = 0; i < 256; i++) {
            System.out.println(" " + i + " " + hist[i]);
        }
    }

    /**
     * Version ELIAS
     *
     * @param bmp
     */
    public void applicationConvolution(Bitmap bmp, int widthMask) {
        int[][] mask = new int[widthMask][widthMask];
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int[][] tabPixR = new int[width][height];
        int[][] tabPixG = new int[width][height];
        int[][] tabPixB = new int[width][height];
        int R, G, B, index, tempR, tempG, tempB, localWeight;
        int[] tabPixels = new int[width * height];
        int weightTotal = 0;
        int pxWidth = widthMask, pxHeight = widthMask;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                mask[i][j] = 1;
                weightTotal++;
            }
        }

        bmp.getPixels(tabPixels, 0, width, 0, 0, width, height);
        //initialisation des tableaux des trois canaux R,G,B des pixels du bitmap passé en parametre
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                index = y * width + x;
                R = (tabPixels[index] >> 16) & 0xff;
                G = (tabPixels[index] >> 8) & 0xff;
                B = tabPixels[index] & 0xff;
                tabPixR[x][y] = R;
                tabPixG[x][y] = G;
                tabPixB[x][y] = B;
            }
        }

        //boucle de l'application du masque
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                index = y * width + x;
                tempR = 0;
                tempG = 0;
                tempB = 0;
                //entre dans le if si le pixel central permet d'appliquer le masque en entier
                if (!(x < pxWidth / 2 || y < pxHeight / 2 || x > width - (pxWidth + 1) / 2 || y > height - (pxHeight + 1) / 2)) {
                    for (int j = 0; j < pxHeight; j++) {
                        for (int i = 0; i < pxWidth; i++) {
                            tempR += tabPixR[x + i - pxWidth / 2][y + j - pxHeight / 2] * mask[i][j];
                            tempG += tabPixG[x + i - pxWidth / 2][y + j - pxHeight / 2] * mask[i][j];
                            tempB += tabPixB[x + i - pxWidth / 2][y + j - pxHeight / 2] * mask[i][j];
                        }
                    }
                    tempR = tempR / weightTotal;
                    tempG = tempG / weightTotal;
                    tempB = tempB / weightTotal;
                } else { //sinon, on applique le traitement d'une bordure
                    localWeight = 0;
                    for (int j = 0; j < pxHeight; j++) {
                        for (int i = 0; i < pxWidth; i++) {
                            if ((x + i - pxWidth / 2) >= 0 && (x + i - pxWidth / 2) < width
                                    && (y + j - pxHeight / 2) >= 0 && (y + j - pxHeight / 2) < height) {
                                tempR += tabPixR[x + i - pxWidth / 2][y + j - pxHeight / 2] * mask[i][j];
                                tempG += tabPixG[x + i - pxWidth / 2][y + j - pxHeight / 2] * mask[i][j];
                                tempB += tabPixB[x + i - pxWidth / 2][y + j - pxHeight / 2] * mask[i][j];
                                localWeight += mask[i][j];
                            }
                        }
                    }
                    if (localWeight == 0) {//si le poid a appliquer est 0 on doit éviter la division par 0, on entre donc dans cette boucle
                        tempR = 0;
                        tempG = 0;
                        tempB = 0;
                    } else {
                        tempR = tempR / localWeight;
                        tempG = tempG / localWeight;
                        tempB = tempB / localWeight;
                    }
                }
                tabPixels[index] = Color.rgb(tempR, tempG, tempB);
            }
        }
        bmp.setPixels(tabPixels, 0, width, 0, 0, width, height);
    }

    /**
     * Colorize ELIAS
     *
     * @param bmp
     * @return
     */
    public void convertImageColorization(Bitmap bmp) {
        Random random = new Random();
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int index;
        int newHue = random.nextInt(359);
        int[] tabPixels = new int[width * height];
        float[] hsv = new float[3];
        int R, G, B;

        bmp.getPixels(tabPixels, 0, width, 0, 0, width, height);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                index = y * width + x;
                //ici on sépare les informations de la couleur du pixel en les répartissant corectement
                R = (tabPixels[index] >> 16) & 0xff;
                G = (tabPixels[index] >> 8) & 0xff;
                B = tabPixels[index] & 0xff;
                //on passe en HSV puis on modifie le "hue" (la teinte)
                Color.RGBToHSV(R, G, B, hsv);
                hsv[0] = newHue;
                tabPixels[index] = Color.HSVToColor(hsv);
            }
        }
        bmp.setPixels(tabPixels, 0, width, 0, 0, width, height);
    }

    /**
     * Egal Histo ELIAS
     *
     * @param bmp
     * @return
     */
    public void applicationEqHistogram(Bitmap bmp) {
        int[] LUT = new int[256];
        int histogramValue[] = new int[256];
        int count, min, max;
        int R, G, B;
        for (int i = 0; i < 256; i++) {
            histogramValue[i] = 0;
            LUT[i] = i;
        }
        count = 0;
        min = 256;
        max = -1;
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int index, valueTemp;
        int[] tabPixels = new int[width * height];
        float[] hsv = new float[3];
        bmp.getPixels(tabPixels, 0, width, 0, 0, width, height);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                index = y * width + x;
                R = (tabPixels[index] >> 16) & 0xff;
                G = (tabPixels[index] >> 8) & 0xff;
                B = tabPixels[index] & 0xff;
                Color.RGBToHSV(R, G, B, hsv);
                valueTemp = (int) (255 * hsv[2]);
                histogramValue[valueTemp]++;
                count++;
                if (valueTemp < min) {
                    min = valueTemp;
                }
                if (valueTemp > max) {
                    max = valueTemp;
                }
            }
        }

        int average = count / 256;
        int tempCount = 0, nextValue = 255;
        for (int i = 0; i < 256; i++) {
            LUT[255 - i] = nextValue;
            tempCount += histogramValue[255 - i];
            if ((tempCount / average) > 1) {
                nextValue -= (tempCount / average);
                if (nextValue < 0) {
                    nextValue = 0;
                }
                tempCount = 0;
            }
        }


        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                index = y * width + x;
                R = (tabPixels[index] >> 16) & 0xff;
                G = (tabPixels[index] >> 8) & 0xff;
                B = tabPixels[index] & 0xff;
                Color.RGBToHSV(R, G, B, hsv);
                valueTemp = (int) (255 * hsv[2]);
                hsv[2] = (float) LUT[valueTemp] / 255;
                tabPixels[index] = Color.HSVToColor(hsv);
            }
        }
        bmp.setPixels(tabPixels, 0, width, 0, 0, width, height);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("CV", "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("CV", "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("CV", "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("CV", "onStop()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("CV", "onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("CV", "onDestroy()");
    }


}