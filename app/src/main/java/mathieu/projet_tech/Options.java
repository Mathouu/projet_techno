package mathieu.projet_tech;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class Options extends AppCompatActivity {

    /**
     * Un switch.
     */
    public Switch night;

    public Button test;

    /**
     * Permet d'initier le switch.
     */
    public void initiateSwitch() {
        night = findViewById(R.id.nuit);
        test = findViewById(R.id.testButton);

    }

    /**
     * Permet d'ajouter une action sur le switch.
     */
    public void addListenerOnSwitch() {
        night.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    modeNight();
                }
                else{
                    modeWhite();
                }
            }
        });

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testPage(view);
            }
        });

    }

    public void testPage(View view) {
        startActivity(new Intent(this, Test.class));
    }

    protected void onCreate(Bundle savedUnstanceState) {
        super.onCreate(savedUnstanceState);
        setContentView(R.layout.options);

        initiateSwitch();
        addListenerOnSwitch();
    }

    /**
     * Permet de mettre la page d'acceuil en noir.
     */
    public void modeNight() {
        ConstraintLayout rl = (ConstraintLayout) findViewById(R.id.optionLayout);
        rl.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
    }

    /**
     * Permet de mettre la page d'acceuil en blanc.
     */
    public void modeWhite() {
        ConstraintLayout rl = (ConstraintLayout) findViewById(R.id.optionLayout);
        rl.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("CV", "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("CV", "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("CV", "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("CV", "onStop()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("CV", "onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("CV", "onDestroy()");
    }


}
